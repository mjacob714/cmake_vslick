/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt or https://cmake.org/licensing for details.  */
#include "cmExtraSlickEditGenerator.h"

#include "cmAlgorithms.h"
#include "cmGeneratedFileStream.h"
#include "cmGeneratorTarget.h"
#include "cmGlobalGenerator.h"
#include "cmLocalGenerator.h"
#include "cmMakefile.h"
#include "cmSourceFile.h"
#include "cmState.h"
#include "cmSystemTools.h"
#include "cmXMLWriter.h"
#include "cmUuid.h"
#include "cmake.h"


cmExtraSlickEditGenerator::cmExtraSlickEditGenerator()
  : cmExternalMakefileProjectGenerator()
{
}

cmExternalMakefileProjectGeneratorFactory*
cmExtraSlickEditGenerator::GetFactory()
{
  static cmExternalMakefileProjectGeneratorSimpleFactory<
    cmExtraSlickEditGenerator>
    factory("SlickEdit", "Generates SlickEdit project and workspace files.");

  if (factory.GetSupportedGlobalGenerators().empty()) {
#if defined(_WIN32)
    factory.AddSupportedGlobalGenerator("MinGW Makefiles");
    factory.AddSupportedGlobalGenerator("NMake Makefiles");
#endif
    factory.AddSupportedGlobalGenerator("Ninja");
    factory.AddSupportedGlobalGenerator("Unix Makefiles");
  }

  return &factory;
}

void cmExtraSlickEditGenerator::Generate()
{
  // iterate through all of the sub-projects
  std::map<std::string, std::vector<cmLocalGenerator*> >::const_iterator it;
  for (it = GlobalGenerator->GetProjectMap().begin();
       it != GlobalGenerator->GetProjectMap().end();
       ++it) {

    const std::vector<cmLocalGenerator*>& lgs = it->second;

    // create a workspace file for each project
    std::string outputDir = lgs[0]->GetCurrentBinaryDirectory();
    std::string workspaceName = lgs[0]->GetProjectName();

    // generate the output filename
    std::string filename = outputDir;
    filename += "/";
    filename += workspaceName;
    filename += ".vpw";
  
    CreateWorkspace(lgs, filename);
  }
}

void cmExtraSlickEditGenerator::CreateWorkspace(const std::vector<cmLocalGenerator*>& lgs,
                                                const std::string& filename)
{
  const cmMakefile* mf = lgs[0]->GetMakefile();
  cmGeneratedFileStream fout(filename.c_str());
  if(!fout) {
    return;
  }

  std::vector<std::string> vpjFiles;

  // iterate through the targets and create vpj files for each
  for(size_t i = 0; i < lgs.size(); ++i) {
    const std::vector<cmGeneratorTarget*>& targets = lgs[i]->GetGeneratorTargets();
    for(size_t j = 0; j < targets.size(); ++j) {

      const std::string& targetName = targets[j]->GetName();
      switch(targets[j]->GetType())
      {
        case cmStateEnums::GLOBAL_TARGET:
          if(0 == strcmp(lgs[i]->GetBinaryDirectory().c_str(), lgs[i]->GetCurrentBinaryDirectory().c_str())) {
            CreateProject(lgs[i]->GetCurrentBinaryDirectory(), targets[j], vpjFiles);
          }
          break;
        case cmStateEnums::UTILITY:
          if((0 == targetName.find("Nightly") && ("Nightly" != targetName)) &&
             (0 == targetName.find("Continuous") && ("Continuous" != targetName)) &&
             (0 == targetName.find("Experimental") && ("Experimental" != targetName))) {
            break;
          }
          
          CreateProject(lgs[i]->GetCurrentBinaryDirectory(), targets[j], vpjFiles);
          break;
        case cmStateEnums::EXECUTABLE:
        case cmStateEnums::STATIC_LIBRARY:
        case cmStateEnums::SHARED_LIBRARY:
        case cmStateEnums::MODULE_LIBRARY:
        case cmStateEnums::OBJECT_LIBRARY:
          CreateProject(lgs[i]->GetCurrentBinaryDirectory(), targets[j], vpjFiles);
          break;
        default:
          break;
      }
    }
  }

  // generate the vpw file contents
  cmXMLWriter xml(fout);
  xml.StartDocument();
  xml.Doctype("Workspace SYSTEM \"http://www.slickedit.com/dtd/vse/10.0/vpw.dtd\"");

  xml.StartElement("Workspace");
  xml.Attribute("Version", "10.0");
  xml.Attribute("VendorName", "SlickEdit");

  xml.StartElement("Projects");

  for(size_t i = 0; i < vpjFiles.size(); ++i) {
    xml.StartElement("Project");
    xml.Attribute("File", vpjFiles[i].c_str());
    xml.EndElement(); //Project
  }

  xml.EndElement(); // Projects

  xml.EndElement(); // Workspace

  xml.EndDocument();
}

void cmExtraSlickEditGenerator::CreateProject(const std::string& path,
                                              const cmGeneratorTarget* target,
                                              std::vector<std::string>& vpjFiles)
{
  // generate the output filename
  std::string filename = path;
  filename += "/";
  filename += target->GetName();
  filename += ".vpj";

  cmGeneratedFileStream fout(filename.c_str());
  if(!fout) {
    return;
  }
  vpjFiles.push_back(filename);

  cmUuid uuidGenerator;

  std::vector<unsigned char> uuidNamespace;
  uuidNamespace.resize(target->GetName().length());
  ::memcpy(&uuidNamespace[0], target->GetName().c_str(), target->GetName().length());

  cmXMLWriter xml(fout);
  xml.StartDocument();
  xml.Doctype("Workspace SYSTEM \"http://www.slickedit.com/dtd/vse/10.0/vpj.dtd\"");

  xml.StartElement("Project");
  xml.Attribute("Version", "10.0");
  xml.Attribute("VendorName", "SlickEdit");
  xml.Attribute("TemplateName", "Makefile");
  xml.Attribute("WorkingDir", path.c_str());
  xml.Attribute("BuildSystem", "");
  xml.Attribute("BuildMakeFile", "Makefile");

  xml.StartElement("Config");
  // if the build type is not specified CMake defaults to Release
  std::vector<std::string> configs;
  target->Makefile->GetConfigurations(configs);
  std::string configName = 0 < configs.size() ? configs[0] : "Release";
  xml.Attribute("Name", configName);
  xml.Attribute("Type", "Makefile");
  xml.Attribute("OutputFile", target->GetName());
  xml.Attribute("ComplierConfigName", "");

  xml.StartElement("Menu");

  // Compile target
  xml.StartElement("Target");
  xml.Attribute("Name", "Compile");
  xml.Attribute("MenuCaption", "&Compile");
  xml.Attribute("CaptureOutputWith", "ProcessBuffer");
  xml.Attribute("SaveOption", "SaveCurrent");
  xml.Attribute("RunFromDir", "%rw");
  xml.StartElement("Exec");
  xml.EndElement(); //Exec
  xml.EndElement(); //Target
  // Build target
  xml.StartElement("Target");
  xml.Attribute("Name", "Build");
  xml.Attribute("MenuCaption", "&Build");
  xml.Attribute("CaptureOutputWith", "ProcessBuffer");
  xml.Attribute("SaveOption", "SaveWorkspaceFiles");
  xml.Attribute("RunFromDir", "%rw");
  xml.StartElement("Exec");
  xml.Attribute("CmdLine", "make -f Makefile all -j");
  xml.EndElement(); //Exec
  xml.EndElement(); //Target
  // Rebuild target
  xml.StartElement("Target");
  xml.Attribute("Name", "Rebuild");
  xml.Attribute("MenuCaption", "&Rebuild");
  xml.Attribute("CaptureOutputWith", "ProcessBuffer");
  xml.Attribute("SaveOption", "SaveWorkspaceFiles");
  xml.Attribute("RunFromDir", "%rw");
  xml.StartElement("Exec");
  xml.Attribute("CmdLine", "make -f Makefile clean all -j");
  xml.EndElement(); //Exec
  xml.EndElement(); //Target
  // Debug target
  xml.StartElement("Target");
  xml.Attribute("Name", "Debug");
  xml.Attribute("MenuCaption", "&Debug");
  xml.Attribute("SaveOption", "SaveNone");
  xml.Attribute("RunFromDir", "%rw");
  xml.StartElement("Exec");
  std::string dbgCmd = "vsdebugio -prog ";
  dbgCmd += target->Makefile->GetCurrentBinaryDirectory();
  dbgCmd += "/";
  dbgCmd += target->GetName();
  xml.Attribute("CmdLine", dbgCmd.c_str());
  xml.EndElement(); //Exec
  xml.EndElement(); //Target
  // Execute target
  xml.StartElement("Target");
  xml.Attribute("Name", "Execute");
  xml.Attribute("MenuCaption", "E&xecute");
  xml.Attribute("SaveOption", "SaveNone");
  xml.Attribute("RunFromDir", "%rw");
  xml.StartElement("Exec");
  xml.Attribute("CmdLine", target->GetName());
  xml.EndElement(); //Exec
  xml.EndElement(); //Target

  xml.EndElement(); //Menu

  xml.EndElement(); //Config

  // Source Files
  xml.StartElement("Files");
  xml.StartElement("Folder");
  xml.Attribute("Name", "Source Files");
  xml.Attribute("Filters", "");
  std::string srcFilesGuid =
    std::string("{") + 
    uuidGenerator.FromSha1(uuidNamespace, std::string("Source Files")) +
    std::string("}");
  xml.Attribute("GUID", srcFilesGuid);

  std::vector<cmSourceFile*> srcs;
  target->GetSourceFiles(srcs, configName);
  for(size_t i = 0; i < srcs.size(); ++i)
  {
    xml.StartElement("F");
    xml.Attribute("N", srcs[i]->GetFullPath());
    xml.EndElement(); //F
  }
  // add the CMakeLists.txt file
  xml.StartElement("F");
  std::string cmakelistspath = target->Makefile->GetCurrentSourceDirectory();
  cmakelistspath += "/CMakeLists.txt";
  xml.Attribute("N", cmakelistspath);
  xml.EndElement(); //F

  xml.EndElement(); //Folder
  xml.EndElement(); //Files

  xml.EndElement(); //Project
  xml.EndDocument();
}

