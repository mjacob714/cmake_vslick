/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt or https://cmake.org/licensing for details.  */
#ifndef cmExtraSlickEditGenerator_h
#define cmExtraSlickEditGenerator_h

#include <cmConfigure.h>

#include "cmExternalMakefileProjectGenerator.h"

#include <vector>
#include <string>

class cmLocalGenerator;
class cmGeneratorTarget;

/** \class cmExtraSlickEditGenerator
 * \brief Write SlickEdit project files for Makefile based projects
 */
class cmExtraSlickEditGenerator : public cmExternalMakefileProjectGenerator
{
public:
  cmExtraSlickEditGenerator();

  static cmExternalMakefileProjectGeneratorFactory* GetFactory();

  void Generate() override;

private:
  void CreateWorkspace(const std::vector<cmLocalGenerator*>& lgs,
                       const std::string& filename);
  void CreateProject(const std::string& path,
                     const cmGeneratorTarget* target,
                     std::vector<std::string>& vpjFiles);
};

#endif
